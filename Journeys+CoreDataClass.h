//
//  Journeys+CoreDataClass.h
//  JourneyTracker
//
//  Created by appkoder on 02/05/2017.
//  Copyright © 2017 appkoder. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject;

NS_ASSUME_NONNULL_BEGIN

@interface Journeys : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Journeys+CoreDataProperties.h"
