//
//  JourneyHistoryViewController.h
//  JourneyTracker
//
//  Created by appkoder on 02/05/2017.
//  Copyright © 2017 appkoder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JourneyHistoryViewController : UIViewController
@property (retain,nonatomic) NSMutableArray *journeyCollections;
@end
