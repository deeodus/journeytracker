//
//  JourneyTrackerViewController.h
//  TrackMyJourney
//
//  Created by appkoder on 28/04/2017.
//  Copyright © 2017 appkoder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "RoundedButton.h"

@interface JourneyTrackerViewController : UIViewController

@property (weak, nonatomic) IBOutlet MKMapView *journeyMap;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *myStartingLocation;
@property (weak, nonatomic) IBOutlet UISwitch *trackSwitch;
- (IBAction)trackJourney:(UISwitch *)sender;
@property (strong, nonatomic)NSMutableArray *locationArrays;
@property (weak, nonatomic) IBOutlet UILabel *motionLabel;
@property BOOL fromHistoryVC;
@property (weak, nonatomic) IBOutlet UILabel *trackLabel;
@property (weak, nonatomic) IBOutlet RoundedButton *endButton;

@end
