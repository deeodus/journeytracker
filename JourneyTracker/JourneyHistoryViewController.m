//
//  JourneyHistoryViewController.m
//  JourneyTracker
//
//  Created by appkoder on 02/05/2017.
//  Copyright © 2017 appkoder. All rights reserved.
//

#import "JourneyHistoryViewController.h"
#import "AppDelegate.h"
#import "HistoryTableViewCell.h"
#import "Journeys+CoreDataProperties.h"
#import "JourneyTrackerViewController.h"
#import "Constants.h"

@interface JourneyHistoryViewController () <UITableViewDataSource, UITableViewDelegate>

@property(weak,nonatomic)AppDelegate *appDelegate;
@property (weak, nonatomic) IBOutlet UITableView *journeyTableView;

@end

@implementation JourneyHistoryViewController
{
    NSDateFormatter *timeFormatter;
    NSDateFormatter *dateFormatter;
    NSMutableArray *locationsArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setTimeStyle:NSDateFormatterMediumStyle];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //obtain the journey record at the selected index path and extract the location array collected for that journey
    Journeys *journey = [_journeyCollections objectAtIndex:indexPath.row];
    locationsArray = [(CLLocation *)journey.locations mutableCopy];
    
    //safety against a nil location array, segue wont be performed
    if (locationsArray) {
        
        [self performSegueWithIdentifier:kShowJourneySegue sender:self];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *tableIdentifier = @"journeyCell";
    
    HistoryTableViewCell *cell = (HistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:tableIdentifier];
    
    Journeys *journey = [_journeyCollections objectAtIndex:indexPath.row];
    cell.dateLabel.text = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:journey.startTime]];
    cell.startTimeLabel.text = [NSString stringWithFormat:@"Start: %@",[timeFormatter stringFromDate:journey.startTime]];
    cell.endTimeLabel.text = [NSString stringWithFormat:@"End: %@",[timeFormatter stringFromDate:journey.endTime]];
    cell.speedLabel.text = [NSString stringWithFormat:@"Speed: %0.2fm/s", journey.speed];
    cell.distanceLabel.text = [NSString stringWithFormat:@"Distance: %0.1fm", journey.distance];
    
    return cell;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _journeyCollections.count;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:kShowJourneySegue]) {
        
        JourneyTrackerViewController *destinationVC = (JourneyTrackerViewController *)[segue destinationViewController];
        destinationVC.locationArrays = locationsArray;
        
        //boolean is used to tell map VC that it's been called from history VC
        destinationVC.fromHistoryVC = YES;
        
    }
}

- (IBAction)dismissVC:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
