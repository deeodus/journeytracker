//
//  RoundedButton.m
//  JourneyTracker
//
//  Created by appkoder on 04/05/2017.
//  Copyright © 2017 appkoder. All rights reserved.
//

#import "RoundedButton.h"

@implementation RoundedButton


- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
        //set some properties of the custom button class
        self.backgroundColor = [UIColor colorWithRed:0.15 green:0.60 blue:0.62 alpha:1.00];
        self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:19];
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.layer.cornerRadius = 5.0f;
        self.clipsToBounds = YES;
    }
    return self;
}
@end
