//
//  Constants.m
//  JourneyTracker
//
//  Created by appkoder on 04/05/2017.
//  Copyright © 2017 appkoder. All rights reserved.
//

#import <Foundation/Foundation.h>

//Name used for Core Data Entity and CloudKit Record Type
NSString* const kCoreDataModelName = @"Journeys";
NSString* const kCloudKitRecordType = @"Journeys";

//Field names for core data and cloud kit records
NSString* const kEndTime = @"endTime";
NSString* const kStartTime = @"startTime";
NSString* const kSpeed = @"speed";
NSString* const kDistance = @"distance";
NSString* const kLocations = @"locations";

//Names for segues
NSString* const kShowJourneySegue = @"showJourney";
NSString* const kHistorySegue = @"history";

//Saved password identifier
NSString* const kPassword = @"password";



