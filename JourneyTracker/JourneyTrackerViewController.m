//
//  JourneyTrackerViewController.m
//  TrackMyJourney
//
//  Created by appkoder on 28/04/2017.
//  Copyright © 2017 appkoder. All rights reserved.
//

#import "JourneyTrackerViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "AppDelegate.h"
#import "Journeys+CoreDataProperties.h"
#import <CloudKit/CloudKit.h>
#import "Constants.h"

@interface JourneyTrackerViewController ()<CLLocationManagerDelegate, MKMapViewDelegate>
@property(weak,nonatomic)AppDelegate *appDelegate;
@end

@implementation JourneyTrackerViewController{
    
    MKPolyline *polyline;
    MKPolylineView *lineView;
    NSDate *startingTime;
    int rangeOldCount;
    int rangeNewCount;
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    if(!_trackSwitch.isOn){
        [_locationManager stopUpdatingLocation];
        _locationManager = nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    _journeyMap.delegate = self;
    
    //checking to see if view is loaded from history or from "new journey"
    if (_fromHistoryVC) {
        
        //if VC is loaded from history, the last object in the saved locations is used to zoom on to the map.
        CLLocation *currentLocation = [_locationArrays lastObject];
        [self zoomToLocation:currentLocation];
        [self drawPathForLocations:_locationArrays];
        
        [_trackSwitch setHidden:YES];
        [_trackLabel setHidden:YES];
        [_endButton setHidden:YES];
        
    }else{
        
        _locationArrays = [NSMutableArray array];
        
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        [_locationManager requestAlwaysAuthorization];
        _locationManager.delegate = self;
        [_locationManager startUpdatingLocation];
        _locationManager.distanceFilter = 3;
        
        _journeyMap.showsUserLocation = YES;
    }
    
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
    //when this method is first called, starting time is nil so zoom is executed so that next round the path drawing begins
    if (!startingTime) {
        
        startingTime = [NSDate date];
        MKUserLocation *userLocation = _journeyMap.userLocation;
        [self zoomToLocation:userLocation.location];
        
    }else{
        
        //during horizontal accuracy calculation, 3 rapid successive inaccurate locations are recorded, 2 seconds wait is done here so that the inacurate readings are not added to user's journey.
        if ([[NSDate date] timeIntervalSinceDate:startingTime] < 2) {
            
            return;
        }
        
        //if tracking is on then the user locations are recorded, otherwise user location are not recorded or drawn on the map.
        if (_trackSwitch.isOn) {
            [_locationArrays addObject:[locations lastObject]];
            [self drawPathForLocations:_locationArrays];
        }
        
    }
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
    NSLog(@"failed");
}

//conveninent method used to zoom to a location provided
-(void)zoomToLocation:(CLLocation *)location{
    
    MKCoordinateRegion region =
    MKCoordinateRegionMakeWithDistance (location.coordinate, 500, 500);
    [_journeyMap setRegion:region animated:NO];
}

//delegate method to zoom to user's current location.
- (void)mapView:(MKMapView *) mapView didUpdateUserLocation: (MKUserLocation *)userLocation
{
    _journeyMap.centerCoordinate = userLocation.location.coordinate;
}


- (void)drawPathForLocations:(NSArray *)locationArray {

    // remove any existing polyline
    [self.journeyMap removeOverlay:polyline];
    
    // create an array of coordinates from locations collected
    CLLocationCoordinate2D coordinates[locationArray.count];
    int i = 0;
    for (CLLocation *locationObject in locationArray) {
        coordinates[i] = locationObject.coordinate;
        i++;
    }
    
    //create a new polyline with all coordinates
    polyline = [MKPolyline polylineWithCoordinates:coordinates count:locationArray.count];
    
    //add polynine to map
    [_journeyMap addOverlay:polyline];
    
}

//mapkit method for rendering polyline onto the app
-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.strokeColor = [UIColor redColor];
    renderer.lineWidth = 2.0;
    
    return renderer;
}

- (IBAction)trackJourney:(UISwitch *)sender {

    if (_trackSwitch.isOn) {
        
        startingTime = [NSDate date];
        [_locationManager startUpdatingLocation];
        [_locationArrays removeAllObjects];
    }else
    {
        [_locationManager stopUpdatingLocation];
        [self saveJourney:_locationArrays];

    }
}

-(void)saveJourney:(NSMutableArray *)journeyArray{
    
    //current date is obtained for end date/time
    NSDate *endTime = [NSDate date];
    
    //first and last cllocation are used to calculate distance, checking to see there are at least 2 objects in the array
    
    if (journeyArray.count < 2) return;
    CLLocation *firstLocation = [journeyArray firstObject];
    CLLocation *lastLocation = [journeyArray lastObject];
    
    float distance = [lastLocation distanceFromLocation:firstLocation];
    
    //time difference between start time and end time
    float timeDifference = [endTime timeIntervalSinceDate:startingTime];
    
    //The data collected saved to Core data
    NSManagedObjectContext *context = _appDelegate.persistentContainer.viewContext;
    
    Journeys* journey = [NSEntityDescription insertNewObjectForEntityForName:kCoreDataModelName inManagedObjectContext:context];
    journey.endTime = endTime;
    journey.startTime = startingTime;
    journey.distance = distance;
    journey.speed = distance/timeDifference;
    journey.locations = [NSArray arrayWithArray:journeyArray];
    
    NSError *error = nil;
    if ([context save:&error] == NO) {
        
        NSLog(@"Error saving context: %@\n%@", [error localizedDescription], [error userInfo]);
    }else{
        
        //If core data save is successful, a copy of the data is saved to CloudKit record
        CKRecord *journeyRecord = [[CKRecord alloc] initWithRecordType:kCloudKitRecordType];

        journeyRecord[kEndTime] = endTime;
        journeyRecord[kStartTime] = startingTime;
        journeyRecord[kDistance] = [NSNumber numberWithFloat:distance];
        journeyRecord[kSpeed] = [NSNumber numberWithFloat:distance/timeDifference];
        journeyRecord[kLocations] = [NSArray arrayWithArray:journeyArray];
        
        [[CKContainer defaultContainer].privateCloudDatabase saveRecord:journeyRecord completionHandler:^(CKRecord *record, NSError *error) {
            
            if (error == nil) {
                NSLog(@"record saved correctly");
            }else{
                NSLog(@"not saved - %@", error.localizedDescription);
            }
        }];
    }
    
}

- (IBAction)dismissVC:(id)sender {
    
    //when the view is dismissed while location is being tracked, save the journey if we re not viewing historical journey
    if(_trackSwitch.isOn && !_fromHistoryVC){
        
        [self saveJourney:_locationArrays];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
