//
//  HistoryTableViewCell.m
//  JourneyTracker
//
//  Created by appkoder on 02/05/2017.
//  Copyright © 2017 appkoder. All rights reserved.
//

#import "HistoryTableViewCell.h"

@implementation HistoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    for (UILabel *cellLabel in _cellLabels){
        
        cellLabel.backgroundColor = [UIColor colorWithRed:0.26 green:0.65 blue:0.67 alpha:1.00];
        cellLabel.clipsToBounds = YES;
        cellLabel.layer.cornerRadius = 8.0f;
        
        cellLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:13];
        
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
