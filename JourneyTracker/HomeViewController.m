//
//  HomeViewController.m
//  JourneyTracker
//
//  Created by appkoder on 03/05/2017.
//  Copyright © 2017 appkoder. All rights reserved.
//

#import "HomeViewController.h"
#import <CoreData/CoreData.h>
#import <CloudKit/CloudKit.h>
#import "Journeys+CoreDataProperties.h"
#import "JourneyHistoryViewController.h"
#import "AppDelegate.h"
#import "Constants.h"

@interface HomeViewController ()

@property(weak,nonatomic)AppDelegate *appDelegate;
@property(strong, nonatomic) NSMutableArray *journeyCollections;

@end

@implementation HomeViewController{
    
    NSUserDefaults *userDefaults;
    NSString *password;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    userDefaults = [NSUserDefaults standardUserDefaults];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    //attempt to get a locally saved password, could be nil
    password = [userDefaults objectForKey:kPassword];
    
    //fetch record from core data model, record can be empty when new
    NSManagedObjectContext *context = _appDelegate.persistentContainer.viewContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kCoreDataModelName];
    _journeyCollections = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    //if the data collection returned from core model is nil/empty then we check cloudkit for records, if the user is logged in and previously saved records then cloud kit will be able to restore those data
    if (!_journeyCollections || _journeyCollections.count == 0) {
        
        //checking default container, searching for all records in the private database for this user.
        CKContainer *defaultContainer = [CKContainer defaultContainer];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"TRUEPREDICATE"];
        CKDatabase *privateDatabase = [defaultContainer privateCloudDatabase];
        CKQuery *query = [[CKQuery alloc] initWithRecordType:kCloudKitRecordType predicate:predicate];
        [privateDatabase performQuery:query inZoneWithID:nil completionHandler:^(NSArray *results, NSError *error) {
            if (!error) {
                
                //Processing any returned records
                for (CKRecord *journeyRecord in results) {
                    
                    NSDate *startTime = (NSDate *)[journeyRecord objectForKey:kStartTime];
                    NSDate *endTime = (NSDate *) [journeyRecord objectForKey:kEndTime];
                    NSNumber *speed = (NSNumber *)[journeyRecord objectForKey:kSpeed];
                    NSNumber *distance = (NSNumber *)[journeyRecord objectForKey:kDistance];
                    NSArray *locations = [journeyRecord objectForKey:kLocations];
                    
                    //Adding the returned records to core data model so that next time we dont query records from cloudKit records.
                    
                    NSManagedObjectContext *context = _appDelegate.persistentContainer.viewContext;
                    
                    Journeys* journey = [NSEntityDescription insertNewObjectForEntityForName:kCoreDataModelName inManagedObjectContext:context];
                    journey.endTime = endTime;
                    journey.startTime = startTime;
                    journey.distance = [distance floatValue];
                    journey.speed = [speed floatValue];
                    journey.locations = [NSArray arrayWithArray:locations];
                    
                    [_journeyCollections addObject:journey];
                    
                    NSError *error = nil;
                    if ([context save:&error] == NO) {
                        
                        NSLog(@"Error saving context: %@\n%@", [error localizedDescription], [error userInfo]);
                    }else{
                        
                    }
                }
                
            } else {
                NSLog(@"%@", error);
            }
        }];
    }
}

- (IBAction)viewJourneyHistory:(UIButton *)sender {
    
    //if there are no records for history, ignore user's click.
    if (_journeyCollections.count == 0 || !_journeyCollections) {
        
        [self showAlert:@"No History" andMessage:@"There are no saved histories for your journey."];
        
        return;
    }
    
    //If the password is not set the user is asked to set new password
    if (!password) {
        
        [self showTextInput:@"New Password" andMessage:@"Please set your password in order to keep your journey history private." isNewPassword:YES buttonTitle:@"Save"];
        
        return;
    }else{
        
        //if the password was previously set, the user is asked to provide it.
        [self showTextInput:@"Type Password" andMessage:@"Please provide your password in order to view your history" isNewPassword:NO buttonTitle:@"OK"];
    }
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:kHistorySegue]) {
        
        JourneyHistoryViewController *destinationVC = (JourneyHistoryViewController *)[segue destinationViewController];
        destinationVC.journeyCollections = _journeyCollections;
    }
}

//convenince method to ask user to provide password or confirm password with text input
-(void)showTextInput:(NSString *)title andMessage:(NSString *)message isNewPassword:(BOOL)new buttonTitle:(NSString *)buttonTitle{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:buttonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        UITextField *textField = [[alert textFields] objectAtIndex:0];
        NSString *typed = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if (new) {
            
            
            if (typed.length >= 3) {
                
                [userDefaults setObject:typed forKey:kPassword];
                [self performSegueWithIdentifier:kHistorySegue sender:self];
            }else{
                [self showAlert:@"Empty Password" andMessage:@"Password must have a minimum of 3 characters in order to continue"];
            }
        }
        else{
            
            if ([typed isEqualToString:password]) {
                
                [self performSegueWithIdentifier:kHistorySegue sender:self];
            }else{
                [self showAlert:@"Wrong password" andMessage:@"You have provided a wrong password, please try again."];
            }
        }
        
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        
        textField.keyboardAppearance = UIKeyboardAppearanceDark;
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:cancel];
    [alert addAction:action];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)showAlert:(NSString *)title andMessage:(NSString *)message{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

@end
