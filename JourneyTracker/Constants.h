//
//  Constants.h
//  JourneyTracker
//
//  Created by appkoder on 04/05/2017.
//  Copyright © 2017 appkoder. All rights reserved.
//

#ifndef Constants_h
#define Constants_h


#endif /* Constants_h */

//Name used for Core Data Entity and CloudKit Record Type
extern NSString* const kCoreDataModelName;
extern NSString* const kCloudKitRecordType;

//Field names for core data and cloud kit records
extern NSString* const kEndTime;
extern NSString* const kStartTime;
extern NSString* const kSpeed;
extern NSString* const kDistance;
extern NSString* const kLocations;

//Names for segues
extern NSString* const kShowJourneySegue;
extern NSString* const kHistorySegue;

//Saved password identifier
extern NSString* const kPassword;

