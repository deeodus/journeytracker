Journey Track App

### General comments: ###

The specification was not entirely clear to me, so I have made this assumptions

* When the user sets tracking to off then the user cllocation value is not saved and the path are not drawn for those locations not tracked
* When the user go from ON to OFF for tracking switch, the locations within that period are saved.

### Known Issues ###
* Sometimes the core data .xcdatamodeld file misbehaves when app is deleted and reinstalled, error “Failed to load model” is displayed. Solution is to delete from project and add it again, and also remember to remove it from “Build phases > Compiled sources”, not sure why I am experiencing this bug.
* CloudKit integration was a first time for me, so I haven’t done all the necessary checks for background execution and appropriate connection issues.

### What could have been done better ###

* Order of journey history – recent journeys at the top
* CloudKit query order is different to core data order, so when data are restored the order is not the same before they were deleted. 
* Password reset would be ideal in case user forgets password
* Core Motion API was initially implemented to add better movement detection, however I found it unreliable (it was slow to detect when I was moving which means some location information will be lost) and did not have more time to improve its accuracy
* I have assumed that the user is currently logged into icloud on their iphone, I haven’t tested when the user isn’t logged in. If a user isn’t logged in I should present icloud authentication so that the data can be backed up.
* CLLocation are saved as array values in a field of a single record (both core data and CloudKit), a better implementation will be to save the locations in a separate table and have a pointer back to the record (one to many relationship).