//
//  Journeys+CoreDataProperties.m
//  JourneyTracker
//
//  Created by appkoder on 02/05/2017.
//  Copyright © 2017 appkoder. All rights reserved.
//

#import "Journeys+CoreDataProperties.h"

@implementation Journeys (CoreDataProperties)

+ (NSFetchRequest<Journeys *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Journeys"];
}

@dynamic startTime;
@dynamic endTime;
@dynamic locations;
@dynamic speed;
@dynamic distance;

@end
