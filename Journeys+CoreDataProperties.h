//
//  Journeys+CoreDataProperties.h
//  JourneyTracker
//
//  Created by appkoder on 02/05/2017.
//  Copyright © 2017 appkoder. All rights reserved.
//

#import "Journeys+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Journeys (CoreDataProperties)

+ (NSFetchRequest<Journeys *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *startTime;
@property (nullable, nonatomic, copy) NSDate *endTime;
@property (nullable, nonatomic, retain) NSObject *locations;
@property (nonatomic) float speed;
@property (nonatomic) float distance;

@end

NS_ASSUME_NONNULL_END
